$(document).ready(function() {
  window.openpane = null;
  window.controlvisible = true;
  $("#ltm").click(function () {
    $(".panelifetext").toggleClass('panelifetextshow');
    $(this).parent().parent().toggleClass('panelifeafter');
    $(this).parent().fadeOut();
    console.log('toggle class');
    window.openpane = true;
  });
    $("#lifeclose").click(function(){

        $(".panelifetextshow").toggleClass('panelifetextshow');
        $(this).parent().parent().parent().toggleClass('panelifeafter');
        $('.middlecontainer').fadeIn();
        window.openpane = false;
    });

    $(".fs-icon").click(function(){
      //var k = $(this).parent().attr('id');
      var k = $(this).closest("div.panelife").prop("id");
      //k = k.charAt(-1);
      k = k.slice(k.length - 1, k.length);

      togllecontrols(k);
    });
});

function togllecontrols(k){
  if(controlvisible){

    $('.ll'+k ).hide();
    $('.toplife').hide();
  }
  else{

      $('.ll'+k ).show();
    $('.toplife').show();

  }
  controlvisible = !controlvisible;
}

function doScene(idf, durata, thook, fasciawidth, tipo, bgsize, bgx, bgy){
  if ( $(window).width() > 992) {

      if(fasciawidth.indexOf("%")>= 0){
        var prc  = parseInt(fasciawidth.substring(0,fasciawidth.indexOf("%")));
        fasciawidth = parseInt($(document).width())*(prc/100);
        //alert("#" + idf + "|" + prc  + " | " + fasciawidth);
      }

      var controller = new ScrollMagic.Controller();
      var target = $("#life");
      var life = document.getElementById("life");
      if(tipo == 'right'){
        TweenMax.set(target, {right:0});
      }else{
        TweenMax.set(target, {left: -500});
      }




      // build scene
      var scene = new ScrollMagic.Scene({
          triggerElement: "#pinned" + idf, duration: durata, offset:10, triggerHook: thook
         })
        .setPin("#pinned" + idf)
        .addIndicators({name: idf + " (duration: '"+ durata +"')"}) // add indicators (requires plugin)
        .addTo(controller)
        //.setClassToggle("#life", "lp-full")
        .on("enter leave ", function (e) {
          if(e.type == "enter"){
            console.log('enter');
            if(tipo == 'right'){
              TweenMax.to("#animate" + idf, 1, {right: 0});
            }else{
              TweenMax.to("#animate" + idf, 1, {left: 0});
            }
            life.style.backgroundSize = bgsize;
            life.style.backgroundPosition = bgx + " " + bgy;

            //ingresso dell llink
            //gestione della freccetta link
            $(".ll" + idf).show();
            console.log('.ll'+idf + ' | show');

          }
          else{
            //definisco l'uscita della fascia
            if(tipo == 'right'){
              TweenMax.to("#animate" + idf, 1,  {right:-fasciawidth});
            }
            else{
              TweenMax.to("#animate" + idf, 1,  {left:-fasciawidth});
            }

            $(".ll" + idf).hide();
            console.log('.ll'+idf + ' | hide');
            try{videojs('my-video1').pause();} catch(err){console.log("video non istanziato");}
            try{videojs('my-video2').pause();} catch(err){console.log("video non istanziato");}
            try{videojs('my-video3').pause();} catch(err){console.log("video non istanziato");}
            try{videojs('my-video4').pause();} catch(err){console.log("video non istanziato");}
            if(window.openpane){
              $(".panelifetextshow").removeClass('panelifetextshow');
              $(".panelife").removeClass('panelifeafter');
              $('.middlecontainer').fadeIn();
            }
            //videojs('my-video4').pause();

          }
        })
      }
    }
