
//$(".navbar-home a").hover(function(){
$('.clickr').hover(
  function(){
    if(!$('.panel-menu').is(":visible")){
      $('.panel-menu').slideToggle();
    }
  }
);
$('#close-menu').click(function(){
  $('.panel-menu').slideToggle();
});

$('.panel-menu').mouseleave(function(){
  if($('.panel-menu').is(":visible")){
    $('.panel-menu').slideToggle();
  }
});


$('#ptaudio').click(
  function(){
    if( $("#video-bg").prop('muted') ) {
      $('#video-bg').prop('muted', false);
      $('#ptaudio').removeClass('vid-muted');
      $('#ptaudio').addClass('vid-audioed');
    }else{
      $('#video-bg').prop('muted', true);
      $('#ptaudio').addClass('vid-muted');
      $('#ptaudio').removeClass('vid-audioed');
    }

  }
);


/*
  gestione del video cover
*/
$( '.control-video' ).click(function() {
  $('#video-cover').prop('muted', false);


    //$( "#video-cover" ).data( "cover", true );
    var vidmp4 = $('#video-cover').data('main-mp4');
    var vidogg = $('#video-cover').data('main-ogg');
    $( "#video-cover" ).data( "covmp4", false );
    var urlmp4 = $('#video-cover source.mp4').attr('src');
    var urlogg = $('#video-cover source.ogg').attr('src');
    $( "#video-cover" ).data( "covmp4", urlmp4 );
    $( "#video-cover" ).data( "covogg", urlogg );
    $( "#video-cover" ).data( "ini", false );


  if($('#video-cover').data('cover')){

    //$( '.control-video' ).fadeOut( "slow");
    $('.control-video').fadeOut();
    $('.video-cover-intro').addClass('video-cover-fs');
    $('#video-cover source.mp4').attr('src', vidmp4);
    $('#video-cover source.ogg').attr('src', vidogg);
    $('#video-cover').get(0).load();
    $('#video-cover').attr('controls', true);
     //$video.attr('controls', '');

    $('#video-cover').get(0).play();
    $('#video-cover').muted = "";
    $( "#video-cover" ).data( "cover", false );
  }
});
$( '#video-cover' ).click(function() {
  if(!$('#video-cover').data('cover')){

    /*
    Attivare questo codice per mostrare nuovamente la copertina
    */
    $('#video-cover').get(0).pause();
    $('#video-cover source.mp4').attr('src', $('#video-cover').data('covmp4'));
    $('#video-cover source.ogg').attr('src', $('#video-cover').data('covogg'));
    $('#video-cover').get(0).load();
    $('#video-cover').get(0).play();

    $('.video-cover-fs').removeClass('video-cover-fs');
    $( "#video-cover" ).data( "cover", true );
    $( "#video-cover" ).data( "continue", true );
    $('.control-video').fadeIn();
    $('#video-cover').prop('muted', true);

  }
});

/*
  gestione del video cover
*/
$( '.skin-video' ).click(function() {
  if($('#video-cover').get(0).paused){
    $('#video-cover').get(0).play();
    $( '.skin-video' ).fadeOut( "slow");

  }
});
$( '#video-cover' ).click(function() {
  if($('#video-cover').get(0).paused){
    $('#video-cover').get(0).pause();
    $('.skin-video' ).show();
  }
});

$( document ).ready(function() {
    $( "#video-cover" ).data( "ini", true );
    $( "#video-cover" ).data( "cover", true );
    $('#video-cover').prop('muted', true);

  try{
    $(".video-quad").get(0).play();
  }catch(err) {
    console.log('elemento non presente');
  }
});

/*homepage*/
/*
$(document).ready(function() {
  $(".himg2").hover(function(){
    $(".hover-image").addClass('hover-image-sel');
    console.log("test");
  }  ,function(){
      $(".hover-image").removeClass('hover-image-sel');
  }
 );
});
*/

$(document).ready(function() {
  $(".navbar-home a").hover(function(){
    /*$(".hover-image").addClass('hover-image-sel');*/
      var num = this.className.split(' ')[0].match(/himg(\d)/)[1];
      try{
        $(".hover-image" + num).addClass('hover-image-sel');
        $(".navbar-home ul.navi a").not(this).animate({'opacity':'0.4'}, 500);
        //$(this).animate({'opacity':'1'}, 0);
      }catch(err) {
        console.log('elemento non presente');
      }

  }  ,function(){

      var num = this.className.split(' ')[0].match(/himg(\d)/)[1];
      try{
        $(".hover-image" + num).removeClass('hover-image-sel');
        $(".navbar-home ul.navi a").animate({'opacity':'1'}, 100);
      }catch(err) {
        console.log('elemento non presente');
      }
  }
 );


});

var frameNumber = 0, // start video at frame 0
// lower numbers = faster playback
playbackConst = 3200,
// get page height from video duration
setHeight = document.getElementById('scrollvideopage');
vid1 = document.getElementById('videoscroll1');
vid2 = document.getElementById('videoscroll2');

if(vid1 != null){
  // dynamically set the page height according to video length
  window.requestAnimationFrame(scrollPlay1);
}
if(vid2 != null){
  window.requestAnimationFrame(scrollPlay2);
}

// Use requestAnimationFrame for smooth playback
function scrollPlay1(){
  var frameNumber  = window.pageYOffset/playbackConst;
  vid1.currentTime  = frameNumber;
  window.requestAnimationFrame(scrollPlay1);
}
function scrollPlay2(){
  var frameNumber  = window.pageYOffset/playbackConst;
  vid2.currentTime  = frameNumber;
  window.requestAnimationFrame(scrollPlay2);
}
